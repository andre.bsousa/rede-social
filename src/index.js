import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Routers from './router'
import { Provider } from 'react-redux'
import store from './store'
import 'antd/dist/antd.css'

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
    <Routers />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
)