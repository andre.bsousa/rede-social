import { Route, Router, Switch } from 'react-router-dom'
import history from './config/history'
import Home from './views/home'


const Routers = () => {
    return (
        <Router history={history}>
            <Switch>
                <Route exact path="/" component={Home} />
            </Switch>
        </Router>
    )
}

export default Routers