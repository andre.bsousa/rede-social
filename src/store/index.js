import { createStore, combineReducers , applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import Reactotron from '../plugins/reactotron-config'

const reducers = combineReducers({})

const middleware = []

const compose = composeWithDevTools(
    applyMiddleware(...middleware),
    Reactotron.createEnhancer()
)

const store = createStore(
    reducers,
    compose
)

export default store

