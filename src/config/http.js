import axios from 'axios'

const localhost = 'http://localhost:8080'

const http = axios.create({
    baseURL: process.env.REACT_APP_API || localhost
})

http.defaults.headers['Content-type'] = 'application/json'

export {
    http
}